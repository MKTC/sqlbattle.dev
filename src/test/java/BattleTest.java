import dev.sqlbattle.backend.model.Battle;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class BattleTest {

	private static final UUID initialPlayerWin = UUID.fromString("b79b341d-1111-4737-9a1e-6cd54fdeff5f");
	private static final UUID initialPlayerSecond = UUID.fromString("7091b328-2222-41ef-8b6d-99008330dcec");
	private static final UUID initialPlayerThird = UUID.fromString("d6016c79-3333-4449-8586-9fda0e16b78c");
	private static final UUID newPlayerA = UUID.fromString("00000000-0000-0000-0000-000000000000");
	private static final UUID newPlayerB = UUID.fromString("2e091426-f8e2-4173-9c05-3fff45544383");
	private static final UUID newPlayerC = UUID.fromString("ffffffff-ffff-ffff-ffff-ffffffffffff");

	public Battle createFirstBattle() {
		return createFirstBattle(2173.91, 2173.91, 2173.91);
	}

	public Battle createFirstBattle(double scoreA, double scoreB, double scoreC) {
		return createFirstBattle(initialPlayerWin, scoreA, initialPlayerSecond, scoreB, initialPlayerThird, scoreC);
	}

	public Battle createFirstBattle(UUID a, double scoreA, UUID b, double scoreB, UUID c, double scoreC) {
		return new Battle(
				1,
				UUID.fromString("650f464f-c81a-4050-a2bf-4daac8139873"),
				"Basic selection",
				"Selecting data from a database is the first step to succes.",
				"data:image/png;base64,",
				"CREATE TABLE users(id, name, salary);INSERT INTO users VALUES(2304, 'Tim', 400);INSERT INTO users VALUES(1996, 'Faab', 250);INSERT INTO users VALUES(8181, 'Jesper', 225);",
				"[{\"id\":2304,\"name\":\"Tim\",\"salary\":400}]",
				"CREATE TABLE users(id, name, salary);INSERT INTO users VALUES(1, 'Aab', 500);INSERT INTO users VALUES(0, 'Noot', 350);INSERT INTO users VALUES(3, 'Mies', 825);",
				"[{\"id\":3,\"name\":\"Mies\",\"salary\":825}]",
				a,
				scoreA,
				b,
				scoreB,
				c,
				scoreC
		);
	}

	@Test
	public void test_battle_insert_higher_score() {
		Battle battle = createFirstBattle();
		UUID newPlayer = newPlayerA;
		double newScore = 2200;

		boolean hasUpdatedScores = battle.checkInsertScore(newPlayer, newScore);

		assertThat("is there a new highscore", hasUpdatedScores, is(true));
		assertThat(battle.cacheWinFirstId, equalTo(newPlayer));
		assertThat(battle.cacheWinFirstScore, is(newScore));
	}

	@Test
	public void test_battle_insert_equal_win_score() {
		// Arrange
		Battle battle = createFirstBattle(2200, 2100, 2000);
		UUID newPlayer = newPlayerA;
		double newScore = 2200;

		// Act
		boolean hasUpdatedScores = battle.checkInsertScore(newPlayer, newScore);

		// Assert
		assertThat("is there a new highscore", hasUpdatedScores, is(true));
		assertThat(battle.cacheWinFirstId, equalTo(initialPlayerWin));
		assertThat(battle.cacheWinFirstScore, is(newScore));
		assertThat(battle.cacheWinSecondId, equalTo(newPlayer));
		assertThat(battle.cacheWinSecondScore, is(newScore));
		assertThat(battle.cacheWinThirdId, equalTo(initialPlayerSecond));
		assertThat(battle.cacheWinSecondScore, is(newScore));
	}

	@Test
	public void test_battle_insert_equal_all_win_score() {
		// Arrange
		Battle battle = createFirstBattle(2200, 2200, 2200);
		UUID newPlayer = newPlayerA;
		double newScore = 2200;

		// Act
		boolean hasUpdatedScores = battle.checkInsertScore(newPlayer, newScore);

		// Assert
		assertThat("is there a new highscore", hasUpdatedScores, is(false));
		assertThat(battle.cacheWinFirstId, equalTo(initialPlayerWin));
		assertThat(battle.cacheWinFirstScore, is(newScore));
		assertThat(battle.cacheWinSecondId, equalTo(initialPlayerSecond));
		assertThat(battle.cacheWinSecondScore, is(newScore));
		assertThat(battle.cacheWinThirdId, equalTo(initialPlayerThird));
		assertThat(battle.cacheWinThirdScore, is(newScore));
	}

	@Test
	public void test_battle_insert_equal_full_win_score() {
		// Arrange
		Battle battle = createFirstBattle(2200, 2200, 2200);
		UUID newPlayer = newPlayerA;
		double newScore = 2300;

		// Act
		boolean hasUpdatedScores = battle.checkInsertScore(newPlayer, newScore);

		// Assert
		assertThat("is there a new highscore", hasUpdatedScores, is(true));
		assertThat(battle.cacheWinFirstId, equalTo(newPlayer));
		assertThat(battle.cacheWinFirstScore, is(newScore));
		assertThat(battle.cacheWinSecondId, equalTo(initialPlayerWin));
		assertThat(battle.cacheWinSecondScore, is(2200D));
		assertThat(battle.cacheWinThirdId, equalTo(initialPlayerSecond));
		assertThat(battle.cacheWinThirdScore, is(2200D));
	}

	@Test
	public void test_battle_insert_equal_partial_win_score() {
		// Arrange
		Battle battle = createFirstBattle(2400, 2200, 2200);
		UUID newPlayer = newPlayerA;
		double newScore = 2300;

		// Act
		boolean hasUpdatedScores = battle.checkInsertScore(newPlayer, newScore);

		// Assert
		assertThat("is there a new highscore", hasUpdatedScores, is(true));
		assertThat(battle.cacheWinFirstId, equalTo(initialPlayerWin));
		assertThat(battle.cacheWinFirstScore, is(2400D));
		assertThat(battle.cacheWinSecondId, equalTo(newPlayer));
		assertThat(battle.cacheWinSecondScore, is(newScore));
		assertThat(battle.cacheWinThirdId, equalTo(initialPlayerSecond));
		assertThat(battle.cacheWinThirdScore, is(2200D));
	}

	@Test
	public void test_battle_insert_equal_bearly_win_score() {
		// Arrange
		Battle battle = createFirstBattle(2400, 2350, 2200);
		UUID newPlayer = newPlayerA;
		double newScore = 2300;

		// Act
		boolean hasUpdatedScores = battle.checkInsertScore(newPlayer, newScore);

		// Assert
		assertThat("is there a new highscore", hasUpdatedScores, is(true));
		assertThat(battle.cacheWinFirstId, equalTo(initialPlayerWin));
		assertThat(battle.cacheWinFirstScore, is(2400D));
		assertThat(battle.cacheWinSecondId, equalTo(initialPlayerSecond));
		assertThat(battle.cacheWinSecondScore, is(2350D));
		assertThat(battle.cacheWinThirdId, equalTo(newPlayer));
		assertThat(battle.cacheWinThirdScore, is(newScore));
	}
}
