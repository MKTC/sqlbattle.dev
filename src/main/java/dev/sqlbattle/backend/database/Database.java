package dev.sqlbattle.backend.database;

import com.zaxxer.hikari.HikariDataSource;
import dev.sqlbattle.backend.general.Logger;
import dev.sqlbattle.backend.general.SafeResultSet;
import dev.sqlbattle.backend.general.ThrowsConsumer;
import dev.sqlbattle.backend.general.ThrowsFunction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

public class Database {

	public static HikariDataSource dataSource;
	public static final ThrowsConsumer<PreparedStatement, SQLException> PREPARE_NOTHING = (ps) -> {
	};

	public static ThrowsConsumer<PreparedStatement, SQLException> fillObject(Object[]... values) {
		return preparedStatement -> {
			if (values == null || values.length == 0) {
				return;
			}
			try {
				int i = 1;
				for (Object[] objs : values) {
					for (Object obj : objs) {
						preparedStatement.setObject(i++, obj);
					}
				}
			} catch (SQLException exception) {
				Logger.warn(exception);
			}
		};
	}


	public static <T> List<T> resultToList(Connection connection, String query,
										   ThrowsFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return resultToList(connection, query, PREPARE_NOTHING, function);
	}

	public static <T> List<T> resultToList(Connection connection, String query,
										   ThrowsConsumer<PreparedStatement, SQLException> prepare,
										   ThrowsFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return resultToList(connection, query, prepare, function, LinkedList::new);
	}

	public static <T> List<T> resultToList(Connection connection, String query,
										   ThrowsConsumer<PreparedStatement, SQLException> prepare,
										   ThrowsFunction<SafeResultSet, T, SQLException> function,
										   Supplier<? extends List<T>> listSupplier) throws SQLException {
		return executeResultQuery(connection, query, prepare, (resultSet) -> {
			if (resultSet == null) {
				return Collections.emptyList();
			}
			List<T> list = listSupplier.get();
			while (resultSet.next()) {
				list.add(function.accept(resultSet));
			}
			return list;
		});
	}

	public static <T> T executeSingleResultQuery(Connection connection, String query,
												 ThrowsFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return executeResultQuery(connection, query, PREPARE_NOTHING, (resultSet) -> {
			if (resultSet != null && resultSet.next()) {
				return function.accept(resultSet);
			}
			return null;
		});
	}

	public static <T> T executeSingleResultQuery(Connection connection, String query,
												 ThrowsConsumer<PreparedStatement, SQLException> prepare,
												 ThrowsFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return executeResultQuery(connection, query, prepare, (resultSet) -> {
			if (resultSet != null && resultSet.next()) {
				return function.accept(resultSet);
			}
			return null;
		});
	}

	public static <T> T executeResultQuery(Connection connection, String query,
										   ThrowsFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return executeResultQuery(connection, query, PREPARE_NOTHING, function);
	}

	public static <T> T executeResultQuery(Connection connection, String query,
										   ThrowsConsumer<PreparedStatement, SQLException> prepare,
										   ThrowsFunction<SafeResultSet, T, SQLException> function) throws SQLException {
		return internalExecuteQuery(connection, query,
				statement -> {
					prepare.accept(statement);
					return function.accept(new SafeResultSet(statement.executeQuery()));
				});
	}

	public static boolean executeQuery(Connection connection, String query) {
		return executeQuery(connection, query, PREPARE_NOTHING);
	}

	public static boolean executeQuery(Connection connection, String query,
									   ThrowsConsumer<PreparedStatement, SQLException> prepare) {
		try {
			return internalExecuteQuery(
					connection,
					query,
					preparedStatement -> {
						prepare.accept(preparedStatement);
						preparedStatement.execute();
						return true;
					});
		} catch (SQLException exception) {
			Logger.warn(exception);
		}
		return false;
	}

	private static <T> T internalExecuteQuery(Connection connection, String query,
											  ThrowsFunction<PreparedStatement, T, SQLException> prepare) throws SQLException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(query);
			return prepare.accept(statement);
		} finally {
			close(statement);
		}
	}

	public static <T> void getConnection(ThrowsConsumer<Connection, SQLException> connectionFunction) {
		getConnection(connectionFunction.toFunction(null));
	}

	public static <T> T getConnection(ThrowsFunction<Connection, T, SQLException> connectionFunction) {
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			return connectionFunction.accept(connection);
		} catch (SQLException exception) {
			Logger.danger(exception);
		} finally {
			close(connection);
		}
		return null;
	}

	private static void close(AutoCloseable closeable) {
		try {
			if (closeable != null) {
				closeable.close();
			}
		} catch (Exception ex) {
			Logger.warn(ex);
		}
	}
}
