package dev.sqlbattle.backend.database.primarykey;

public class StringIntKey implements Key {

	private final String keyA;
	private final int keyB;

	public StringIntKey(String keyA, int keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}

	public String getKeyA() {
		return keyA;
	}

	public int getKeyB() {
		return keyB;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB() };
	}
}
