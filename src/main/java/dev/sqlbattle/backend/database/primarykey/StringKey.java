package dev.sqlbattle.backend.database.primarykey;

public class StringKey implements Key {

	private final String key;

	public StringKey(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKey() };
	}
}
