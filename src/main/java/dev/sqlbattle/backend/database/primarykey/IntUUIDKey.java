package dev.sqlbattle.backend.database.primarykey;

import java.util.UUID;

public class IntUUIDKey implements Key {

	private final int keyA;
	private final UUID keyB;

	public IntUUIDKey(int keyA, UUID keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}

	public int getKeyA() {
		return keyA;
	}

	public UUID getKeyB() {
		return keyB;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB() };
	}
}
