package dev.sqlbattle.backend.database.primarykey;

import java.util.UUID;

public class RelationKey implements Key {

	private final UUID id;
	private final String friendship;
	private final UUID from;
	private final UUID to;

	public RelationKey(UUID id, String friendship, UUID from, UUID to) {
		this.id = id;
		this.friendship = friendship;
		this.from = from;
		this.to = to;
	}

	public UUID getId() {
		return id;
	}

	public String getFriendship() {
		return friendship;
	}

	public UUID getFrom() {
		return from;
	}

	public UUID getTo() {
		return to;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getId(), getFriendship(), getFrom(), getTo() };
	}

}
