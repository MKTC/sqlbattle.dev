package dev.sqlbattle.backend.database.primarykey;

import java.util.UUID;

public class UUIDStringKey implements Key {

	private final UUID keyA;
	private final String keyB;

	public UUIDStringKey(UUID keyA, String keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}

	public UUID getKeyA() {
		return keyA;
	}

	public String getKeyB() {
		return keyB;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB() };
	}
}
