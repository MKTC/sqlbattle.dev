package dev.sqlbattle.backend.database.primarykey;

public class DoubleIntKey implements Key {

	private final int keyA;
	private final int keyB;

	public DoubleIntKey(int keyA, int keyB) {
		this.keyA = keyA;
		this.keyB = keyB;
	}

	public int getKeyA() {
		return keyA;
	}

	public int getKeyB() {
		return keyB;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB() };
	}
}
