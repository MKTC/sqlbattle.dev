package dev.sqlbattle.backend.database.primarykey;

import java.util.UUID;

public class TrippleUUIDKey implements Key {

	private final UUID keyA;
	private final UUID keyB;
	private final UUID keyC;

	public TrippleUUIDKey(UUID keyA, UUID keyB, UUID keyC) {
		this.keyA = keyA;
		this.keyB = keyB;
		this.keyC = keyC;
	}

	public UUID getKeyA() {
		return keyA;
	}

	public UUID getKeyB() {
		return keyB;
	}

	public UUID getKeyC() {
		return keyC;
	}

	@Override
	public Object[] getKeys() {
		return new Object[] { getKeyA(), getKeyB(), getKeyC() };
	}
}
