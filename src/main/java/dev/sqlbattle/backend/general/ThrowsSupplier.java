package dev.sqlbattle.backend.general;

import java.util.function.Function;

public interface ThrowsSupplier<T, Ex extends Throwable> {

	T supply() throws Ex;

	default <T2> ThrowsFunction<T, T, Ex> toFunction() {
		return (t) -> supply();
	}

	default ThrowsSupplier<T[], Ex> andThen(ThrowsSupplier<T, Ex> after, Function<Integer, T[]> toArray) {
		return () -> {
			T[] array = toArray.apply(2);
			array[0] = supply();
			array[1] = after.supply();
			return array;
		};
	}
}
