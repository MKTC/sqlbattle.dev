package dev.sqlbattle.backend.general;

import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.binary.Base64;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StringHelper {

	public static String makeCorrectLength(String input, int length) {
		StringBuilder inputBuilder = new StringBuilder(input);
		while (inputBuilder.length() < length) {
			inputBuilder.append(' ');
		}
		return inputBuilder.subSequence(0, length).toString();
	}

	public static String decodeBase64(String base64) {
		return new String(Base64.decodeBase64(base64), Charsets.UTF_8);
	}

	public static String getHostname() {
		InetAddress localhost;
		try {
			localhost = InetAddress.getLocalHost();
		} catch (UnknownHostException ex) {
			Logger.error(ex);
			return "???";
		}
		return localhost.getHostName();
	}

	public static String parseSafeName(String raw) {
		return raw.replaceAll("/[^\\p{N}\\p{L}_-]*/mu", raw);
	}

	public static Date parseDate(String dateString) {
		if (dateString.length() != 10 ||
				!dateString.matches("^([0-2][0-9]|(3)[0-1])(-)(((0)[0-9])|((1)[0-2]))(-)\\d{4}$")) {
			return null;
		}
		try {
			Date date = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
			if (date.getTime() > System.currentTimeMillis() ||
					date.getTime() < -2208992400000L/*01-01-1900*/) {
				throw new ParseException(dateString, 0);
			}
			return new Date(date.getTime() + (WaitForIt.HOUR_IN_MILLIS * 12));
		} catch (ParseException ex) {
			return null;
		}
	}
}
