package dev.sqlbattle.backend.general.security;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.UUID;

public class MD5 implements HashFunction {

	private boolean appendBefore;

	public MD5(boolean appendBefore) {
		this.appendBefore = appendBefore;
	}

	public String apply(String s) {
		return DigestUtils.md5Hex(s) + (appendBefore ? s : "");
	}

	@Override
	public String apply(String s, UUID user) {
		return DigestUtils.md5Hex(s) + (appendBefore ? s : "");
	}
}
