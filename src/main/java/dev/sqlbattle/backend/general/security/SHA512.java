package dev.sqlbattle.backend.general.security;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.UUID;

public class SHA512 implements HashFunction {

	@Override
	public String apply(String s, UUID u) {
		return DigestUtils.sha512Hex(s);
	}
}
