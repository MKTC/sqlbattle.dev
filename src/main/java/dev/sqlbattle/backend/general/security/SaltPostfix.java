package dev.sqlbattle.backend.general.security;

import java.util.UUID;

public class SaltPostfix implements HashFunction {

	private final String salt;

	public SaltPostfix(String salt) {
		this.salt = salt;
	}

	@Override
	public String apply(String s, UUID u) {
		return s + salt;
	}
}
