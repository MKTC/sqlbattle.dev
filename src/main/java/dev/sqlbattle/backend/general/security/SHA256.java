package dev.sqlbattle.backend.general.security;

import org.apache.commons.codec.digest.DigestUtils;

import java.util.UUID;

public class SHA256 implements HashFunction {

	@Override
	public String apply(String s, UUID u) {
		return DigestUtils.sha256Hex(s);
	}
}
