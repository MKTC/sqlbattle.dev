package dev.sqlbattle.backend.general;

import java.sql.SQLException;

@FunctionalInterface
public interface TriFunction<T, T2, T3, R> {

	R apply(T t, T2 t2, T3 t3) throws SQLException;
}
