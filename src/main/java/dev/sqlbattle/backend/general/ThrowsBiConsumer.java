package dev.sqlbattle.backend.general;

public interface ThrowsBiConsumer<T, S, Ex extends Throwable> {

	void accept(T t, S s) throws Ex;

	default <T2> ThrowsBiFunction<T, S, T2, Ex> toFunction(T2 functionResponse) {
		return (t, s) -> {
			accept(t, s);
			return functionResponse;
		};
	}

	default ThrowsBiConsumer<T, S, Ex> andThen(ThrowsBiConsumer<T, S, Ex> after) {
		return (T t, S s) -> {
			accept(t, s);
			after.accept(t, s);
		};
	}
}
