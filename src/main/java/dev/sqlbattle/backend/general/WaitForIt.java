package dev.sqlbattle.backend.general;

public class WaitForIt {

	public final static int SECOND_IN_MILLIS = 1000;
	public final static int MINUTE_IN_MILLIS = 60 * 1000;
	public final static int HOUR_IN_MILLIS = 60 * 60 * 1000;
	public final static int DAY_IN_MILLIS = 24 * 60 * 60 * 1000;

	public static void sleep(int seconds) {
		sleepMillis(seconds * SECOND_IN_MILLIS);
	}

	public static void sleepMillis(long millis) {
		try {
			if (millis < 0) {
				return;
			}
			Thread.sleep(millis);
		} catch (Exception exception) {
			throw new RuntimeException(exception);
		}
	}
}
