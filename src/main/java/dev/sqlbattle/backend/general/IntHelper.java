package dev.sqlbattle.backend.general;

public class IntHelper {

	public static int clampBetween(int input, int min, int max) {
		return Math.max(min, Math.min(input, max));
	}
}
