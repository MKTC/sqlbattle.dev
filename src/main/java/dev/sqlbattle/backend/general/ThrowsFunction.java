package dev.sqlbattle.backend.general;

public interface ThrowsFunction<T1, T2, Ex extends Throwable> {

	T2 accept(T1 t1) throws Ex;
}
