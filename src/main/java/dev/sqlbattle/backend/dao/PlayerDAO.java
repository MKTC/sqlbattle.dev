package dev.sqlbattle.backend.dao;

import dev.sqlbattle.backend.database.Database;
import dev.sqlbattle.backend.database.SQLBuilder;
import dev.sqlbattle.backend.database.primarykey.KeySet;
import dev.sqlbattle.backend.database.primarykey.StringKey;
import dev.sqlbattle.backend.general.SafeResultSet;
import dev.sqlbattle.backend.model.Player;
import dev.sqlbattle.backend.model.PlayerWithLogin;
import dev.sqlbattle.backend.model.PlayerWithScore;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class PlayerDAO extends DatabaseAccessObject<Player> {

	private PlayerDAO() {
	}

	@Override
	public String getTableName() {
		return "player";
	}

	@Override
	public Player fromResultSet(SafeResultSet resultSet) {
		return new Player(
				UUIDFromResultSet(resultSet, "uuid"),
				resultSet.getString("username"),
				resultSet.getString("password"),
				resultSet.getString("image"),
				resultSet.getString("job_title"),
				resultSet.getString("website"),
				resultSet.getString("git_server"),
				resultSet.getString("git"),
				resultSet.getString("twitter"),
				resultSet.getString("hyven"),
				resultSet.getString("codepen")
		);
	}

	public PlayerWithLogin fromResultSetWithLogin(SafeResultSet resultSet) {
		return new PlayerWithLogin(
				UUIDFromResultSet(resultSet, "uuid"),
				resultSet.getString("username"),
				resultSet.getString("password"),
				resultSet.getString("image"),
				resultSet.getString("job_title"),
				resultSet.getString("website"),
				resultSet.getString("git_server"),
				resultSet.getString("git"),
				resultSet.getString("twitter"),
				resultSet.getString("hyven"),
				resultSet.getString("codepen")
		);
	}

	public PlayerWithScore fromResultSetWithScore(SafeResultSet resultSet) {
		return new PlayerWithScore(
				UUIDFromResultSet(resultSet, "uuid"),
				resultSet.getString("username"),
				resultSet.getString("password"),
				resultSet.getString("image"),
				resultSet.getString("job_title"),
				resultSet.getString("website"),
				resultSet.getString("git_server"),
				resultSet.getString("git"),
				resultSet.getString("twitter"),
				resultSet.getString("hyven"),
				resultSet.getString("codepen"),
				resultSet.getDouble("score")
		);
	}

	public Optional<Player> findUserByUsername(Connection connection, String username) throws SQLException {
		return findFirstWithKey(connection, new KeySet(new StringKey(username), "username"));
	}

	public Optional<PlayerWithLogin> findLoginUserByUsername(Connection connection, String username) throws SQLException {
		KeySet key = new KeySet(new StringKey(username), "username");
		return Database.resultToList(connection,
				SQLBuilder.readAll(this.getTableName(), getColumns()) + SQLBuilder.whereUnique(key.getKeyColumns()),
				Database.fillObject(key.getKeyValues()),
				this::fromResultSetWithLogin
		).stream().findFirst();
	}

	public List<PlayerWithScore> getPlayersSortedByScore(Connection connection, int limit) throws SQLException {
		return Database.resultToList(
				connection,
				"SELECT SUM(score) AS total_score, p.*\n" +
						"FROM (\n" +
						"\tSELECT p.uuid, MAX(s.score) AS score\n" +
						"\tFROM battle_submit s\n" +
						"\tLEFT JOIN player p\n" +
						"\tON s.player_id = p.uuid\n" +
						"\tGROUP BY s.battle_id, p.uuid\n" +
						") AS submits\n" +
						"LEFT JOIN player p\n" +
						"ON submits.uuid = p.uuid\n" +
						"GROUP BY score\n" +
						"ORDER BY total_score DESC\n" +
						"LIMIT " + limit,
				this::fromResultSetWithScore
		);
	}
}
