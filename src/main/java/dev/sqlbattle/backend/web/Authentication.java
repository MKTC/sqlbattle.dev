package dev.sqlbattle.backend.web;

import dev.sqlbattle.backend.dao.PlayerDAO;
import dev.sqlbattle.backend.general.StringHelper;
import dev.sqlbattle.backend.general.security.HashSequence;
import dev.sqlbattle.backend.model.Player;
import spark.Request;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Optional;

public class Authentication {

	public static Optional<Player> login(Connection connection, Request request) throws SQLException {
		String authorization = Route.headerOrQueryOrParamOrBody(request, "Authorization");

		String[] bearer = parseBearer(authorization);
		if (bearer == null) {
			return Optional.empty();
		}
		String username = bearer[0];
		String password = bearer[1];

		Optional<Player> optionalUser = PlayerDAO.getInstance(PlayerDAO.class)
				.findUserByUsername(connection, username);
		if (!optionalUser.isPresent()) {
			return Optional.empty();
		}
		Player user = optionalUser.get();
		return checkPassword(user, password);
	}

	public static Optional<Player> checkPassword(Player user, String password) {
		if (user.getUUID().toString().equals(password)
				|| HashSequence.comparePassword(user.getUUID(), password, user.password)) {
			return Optional.of(user);
		}
		return Optional.empty();
	}


	private static String[] parseBearer(String rawBearer) {
		if (rawBearer != null && !rawBearer.trim().isEmpty() && rawBearer.length() > 9 && rawBearer.toLowerCase().startsWith("bearer ")) {
			rawBearer = rawBearer.substring(7);
			rawBearer = StringHelper.decodeBase64(rawBearer);
			if (rawBearer.contains(":")) {
				String[] bearer = rawBearer.split(":", 2);
				if (bearer.length == 2 && !bearer[0].trim().isEmpty() && !bearer[1].trim().isEmpty()) {
					return bearer;
				}
			}
		}
		return null;
	}
}
