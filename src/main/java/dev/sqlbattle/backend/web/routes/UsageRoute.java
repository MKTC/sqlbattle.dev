package dev.sqlbattle.backend.web.routes;

import dev.sqlbattle.backend.dao.UsageDAO;
import dev.sqlbattle.backend.database.Database;
import dev.sqlbattle.backend.model.Usage;
import dev.sqlbattle.backend.web.Route;
import org.apache.commons.collections4.MultiValuedMap;
import spark.Request;
import spark.Response;
import spark.route.HttpMethod;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;

public class UsageRoute implements Route {

	private static final String usageSince = "24 hours ago";

	@Override
	public MultiValuedMap<String, HttpMethod> getRequests() {
		return new RouteRequestBuilder()
				.add(HttpMethod.get, "/usage")
				.add(HttpMethod.get, "/usage/:uuid")
				.build();
	}

	@Override
	public Integer call(HttpMethod method, Request request, Response response) {
		return Database.getConnection(connection -> {
			cleanup(connection);
			registerVisit(connection, request);
			return UsageDAO.getInstance(UsageDAO.class)
					.countSince(connection, usageSince);
		});
	}

	public static void cleanup(Connection connection) throws SQLException {
		UsageDAO.getInstance(UsageDAO.class)
				.removeSince(connection, usageSince);
	}

	public static void registerVisit(Connection connection, Request request) throws SQLException {
		String uuidString = Route.headerOrQueryOrParamOrBody(request, "uuid");
		if (uuidString == null
				|| !uuidString.matches("[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}")) {
			uuidString = "00000000" + UUID.randomUUID().toString().substring(8);
		}
		UUID uuid = UUID.fromString(uuidString);
		String userAgent = Route.headerOrQueryOrParamOrBody(request, "User-Agent");
		if (userAgent == null) {
			userAgent = "unknown";
		}
		Usage usage = new Usage(uuid, Route.getClientIp(request), userAgent, null);
		UsageDAO.getInstance(UsageDAO.class)
				.create(connection, usage);
	}
}
