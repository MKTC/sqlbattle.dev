package dev.sqlbattle.backend.web.routes;

import dev.sqlbattle.backend.general.Settings;
import dev.sqlbattle.backend.web.Route;
import org.apache.commons.collections4.MultiValuedMap;
import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;
import spark.Request;
import spark.Response;
import spark.route.HttpMethod;

import java.util.HashMap;
import java.util.Map;

public class LandingRoute implements Route {

	private final Map<String, JtwigTemplate> templates = new HashMap<>();

	@Override
	public MultiValuedMap<String, HttpMethod> getRequests() {
		String[] routes = {
				/*"/",*/ "index", "self", "signup", "battle", "battle/*"
		};
		String[] extensions = {
				"html", "xhtml", "htm", "php", "asp", "pl", "cgi"
		};

		RouteRequestBuilder builder = new RouteRequestBuilder(40);
		builder.add(HttpMethod.get, "/");
		for (String route : routes) {
			builder.add(HttpMethod.get, String.format("/%s", route));
			for (String extension : extensions) {
				builder.add(HttpMethod.get, String.format("/%s.%s", route, extension));
			}
			templates.put(route, JtwigTemplate.classpathTemplate("twig/" + route + ".twig"));
		}

		return builder.build();
	}

	@Override
	public String call(HttpMethod method, Request request, Response response) {
		String requestString = request.matchedPath()
				.split("\\.")[0]
				.replaceFirst("/+", "")
				.replaceAll("/\\*", "");
		if (requestString.equals("")) {
			requestString = "index";
		}

		boolean debug = Settings.SETTINGS.getBoolean("server.debug");
		if (debug) {
			return JtwigTemplate.classpathTemplate("twig/" + requestString + ".twig")
					.render(new JtwigModel());
		}
		return templates.get(requestString).render(new JtwigModel()).trim();
	}
}
