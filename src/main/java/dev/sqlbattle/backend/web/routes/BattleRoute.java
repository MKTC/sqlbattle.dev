package dev.sqlbattle.backend.web.routes;

import dev.sqlbattle.backend.dao.BattleDAO;
import dev.sqlbattle.backend.dao.BattleSubmitDAO;
import dev.sqlbattle.backend.dao.PlayerDAO;
import dev.sqlbattle.backend.database.Database;
import dev.sqlbattle.backend.database.primarykey.IntKey;
import dev.sqlbattle.backend.database.primarykey.KeySet;
import dev.sqlbattle.backend.database.primarykey.UUIDKey;
import dev.sqlbattle.backend.general.SafeTry;
import dev.sqlbattle.backend.model.Battle;
import dev.sqlbattle.backend.model.BattleSubmit;
import dev.sqlbattle.backend.web.Route;
import org.apache.commons.collections4.MultiValuedMap;
import spark.Request;
import spark.Response;
import spark.route.HttpMethod;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import static dev.sqlbattle.backend.model.BattleSubmit.*;

public class BattleRoute implements Route {

	@Override
	public MultiValuedMap<String, HttpMethod> getRequests() {
		return new RouteRequestBuilder()
				.add(HttpMethod.get, "/battles")
				.add(HttpMethod.get, "/battles-new")
				.add(HttpMethod.get, "/battles/:page")
				.add(HttpMethod.get, "/battle-get/:id")
				.add(HttpMethod.get, "/leaderboard/:id")
				.build();
	}

	@Override
	public Object call(HttpMethod method, Request request, Response response) {
		switch (request.uri()) {
			case "/battles":
				return getLatestBattles();
			case "/battles-new":
				return getOldestBattles();
		}
		if (request.uri().startsWith("/battles/")) {
			return getBattlesPaged(request);
		}
		if (request.uri().startsWith("/battle-get/")) {
			return getBattle(request);
		}
		if (request.uri().startsWith("/leaderboard/")) {
			return getBattleLeaderboard(request);
		}

		throw new RuntimeException("Method not implemented exception");
	}

	public Battle getBattle(Request request) {
		String idString = request.params("id");
		int id = SafeTry.execute(() -> Integer.parseInt(idString), e -> 0);

		return Database.getConnection(connection -> {
			return BattleDAO.getInstance(BattleDAO.class)
					.findFirstWithKey(connection, new KeySet(new IntKey(id), "id"))
					.orElse(null);
		});
	}

	public List<PublicBattleSubmitView> getBattleLeaderboard(Request request) {
		String idString = request.params("id");
		String pageString = request.queryParams("page");

		int id = SafeTry.execute(() -> Integer.parseInt(idString), e -> 0);
		int page = SafeTry.execute(() -> Integer.parseInt(pageString), e -> 0);

		return getLeaderboardPaged(id, page);
	}

	public List<Battle> getBattlesPaged(Request request) {
		String pageString = request.params("page");
		int page = SafeTry.execute(() -> Integer.parseInt(pageString), e -> 0);

		return getBattlesPaged(page, "id DESC");
	}

	public List<Battle> getLatestBattles() {
		return getBattlesPaged(0, "id DESC");
	}

	public List<Battle> getOldestBattles() {
		return getBattlesPaged(0, "id ASC");
	}

	private List<Battle> getBattlesPaged(int page, String orderBy) {
		int pageSize = 10;

		return Database.getConnection(connection -> {
			List<Battle> battles = BattleDAO.getInstance(BattleDAO.class)
					.getXOffsetOrderBy(connection, pageSize, page * pageSize, orderBy);
			for (Battle battle : battles) {
				fillBattle(connection, battle);
			}

			return battles;
		});
	}

	private void fillBattle(Connection connection, Battle battle) throws SQLException {
		PlayerDAO playerDAO = PlayerDAO.getInstance(PlayerDAO.class);

		if (battle.submitted_by != null) {
			battle.setSubmittedBy(
					playerDAO.findFirstWithKey(connection, new KeySet(new UUIDKey(battle.submitted_by), "uuid"))
							.orElse(null)
			);
		}
		if (battle.cacheWinFirstId != null) {
			battle.setCacheWinFirst(
					playerDAO.findFirstWithKey(connection, new KeySet(new UUIDKey(battle.cacheWinFirstId), "uuid"))
							.orElse(null)
			);
		}
		if (battle.cacheWinSecondId != null) {
			battle.setCacheWinSecond(
					playerDAO.findFirstWithKey(connection, new KeySet(new UUIDKey(battle.cacheWinSecondId), "uuid"))
							.orElse(null)
			);
		}
		if (battle.cacheWinThirdId != null) {
			battle.setCacheWinThird(
					playerDAO.findFirstWithKey(connection, new KeySet(new UUIDKey(battle.cacheWinThirdId), "uuid"))
							.orElse(null)
			);
		}
	}

	private List<PublicBattleSubmitView> getLeaderboardPaged(int id, int page) {
		int pageSize = 10;

		//Er wordt 1 element meer terug gegeven zodat je hiermee kan controleren of er een volgend pagina is.
		return Database.getConnection(connection -> {
			List<BattleSubmit> scores = BattleSubmitDAO.getInstance(BattleSubmitDAO.class)
					.getLeaderboard(connection, id, pageSize + 1, pageSize * page);
			for (BattleSubmit battleSubmit : scores) {
				fillBattleSubmit(connection, battleSubmit);
			}

			return scores.stream()
					.map(PublicBattleSubmitView::new)
					.collect(Collectors.toList());
		});
	}

	private void fillBattleSubmit(Connection connection, BattleSubmit battleSubmit) throws SQLException {
		PlayerDAO playerDAO = PlayerDAO.getInstance(PlayerDAO.class);

		if (battleSubmit.playerId != null) {
			battleSubmit.setPlayer(
					playerDAO.findFirstWithKey(connection, new KeySet(new UUIDKey(battleSubmit.playerId), "uuid"))
							.orElse(null)
			);
		}
	}
}
