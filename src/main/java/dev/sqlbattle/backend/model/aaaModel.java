package dev.sqlbattle.backend.model;

import dev.sqlbattle.backend.database.primarykey.KeySet;
import dev.sqlbattle.backend.database.primarykey.StringKey;

@SuppressWarnings("WeakerAccess")
public class aaaModel implements Model<aaaModel> {

	public KeySet keySet;

	public aaaModel(String key) {
		this.keySet = new KeySet(new StringKey(key), "id");
	}

	@Override
	public KeySet getPrimaryKey() {
		return keySet;
	}
}
