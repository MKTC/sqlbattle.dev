package dev.sqlbattle.backend.sqlite;

import com.google.gson.Gson;
import dev.sqlbattle.backend.database.Database;
import dev.sqlbattle.backend.general.*;
import dev.sqlbattle.backend.model.Battle;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.sql.*;
import java.util.*;
import java.util.function.BiConsumer;

public class SQLiteInitializer implements Initializer {

	private static final Gson GSON = new Gson();

	@Override
	public void init() {
		Class<?> clazz = SafeTry.execute(() ->
				Class.forName("org.sqlite.JDBC")
		);
		if (clazz == null) {
			throw new RuntimeException("Cannot find class org.sqlite.JDBC");
		}
		//noinspection ResultOfMethodCallIgnored
		new File("./db").mkdirs();
		SafeTry.execute(() -> FileUtils.cleanDirectory(new File("./db")));
	}

	public static void getScoreWith(Battle battle, String sql, BiConsumer<String, Double> result) {
		executeSet(battle.dataset, sql, battle.previewResult, (resultSetData, preview) -> {
			executeSet(battle.testDataset, sql, battle.testResult, (resultSetTest, previewTest) -> {
				result.accept(preview, (Math.round(10000000D / sql.length()) / 100D));
			});
		});
	}

	private static void executeSet(String dataset, String userSQL, String expectedResult,
								   ThrowsBiConsumer<SafeResultSet, String, SQLException> after) {
		createDatabase(UUID.randomUUID().toString(), connection -> {
			executeBulkSQL(connection, dataset, c -> {
			});
			executeBulkSQL(connection, userSQL, resultSet -> {
				List<Map<String, Object>> resultAsObject = resultSetToMaps(resultSet);

				String gson = GSON.toJson(resultAsObject);
				if (!compareResult(gson, expectedResult)) {
					throw new RuntimeException("Result did not match result: " + expectedResult + " ~ " + gson);
				}
				after.accept(resultSet, gson);
			});
		});
	}

	private static boolean compareResult(String gson, String expectedResult) {
		return gson.equals(expectedResult);
	}

	private static List<Map<String, Object>> resultSetToMaps(SafeResultSet safeResultSet) throws SQLException {
		List<Map<String, Object>> maps = new ArrayList<>();
		while (safeResultSet.next()) {
			Map<String, Object> map = new LinkedHashMap<>();
			ResultSetMetaData metaData = safeResultSet.getMetaData();
			int columnCount = metaData.getColumnCount();
			for (int columnNumber = 1; columnNumber <= columnCount; columnNumber++) {
				map.put(metaData.getColumnLabel(columnNumber), safeResultSet.getObject(columnNumber));
			}
			maps.add(map);
		}
		return maps;
	}

	//Following https://www.sqlite.org/syntax/select-stmt.html
	private static boolean isSelectStmt(String stmt) {
		stmt = stmt.toUpperCase();
		return stmt.startsWith("SELECT") || stmt.startsWith("WITH") || stmt.startsWith("VALUES");
	}

	private static void executeBulkSQL(Connection connection, String bulkSQL, ThrowsConsumer<SafeResultSet, SQLException> consumer) throws SQLException {
		String[] split = splitSQL(bulkSQL);
		for (int i = 0; i < split.length; i++) {
			String statement = split[i].trim();
			if (statement.isEmpty() || statement.toUpperCase().matches("/(?<![`'\"])ATTACH/g")) {
				continue;
			}
			boolean isLastSelect = isSelectStmt(statement);
			for (int j = i + 1; j < split.length; j++) {
				if (isSelectStmt(split[j].trim())) {
					isLastSelect = false;
					break;
				}
			}
			if (isLastSelect) {
				Database.executeResultQuery(connection, statement, consumer.toFunction(null));
				break;
			} else {
				if (!Database.executeQuery(connection, statement)) {
					throw new RuntimeException("Error with statement `" + statement + "`");
				}
			}
		}
	}

	private static String[] splitSQL(String bulkSQL) {
		return bulkSQL.split(";");
	}

	private static void createDatabase(String uniqueId, ThrowsConsumer<Connection, SQLException> connectionConsumer) {
		String file = "./db/" + uniqueId + ".db";
		String url = "jdbc:sqlite:" + file;

		try (Connection connection = DriverManager.getConnection(url)) {
			if (connection == null) {
				throw new SQLException("Cannot make this connection " + url);
			}
			connectionConsumer.accept(connection);
		} catch (SQLException e) {
			Logger.error(e);
		} finally {
			new File(file).delete();
		}
	}
}
